﻿using System;
using System.Diagnostics;
using DiagDbg = System.Diagnostics.Debug;
//using System.Linq;

namespace BasicProcInfo.Debug
{
    public static class Asserts
    {
        private static StackFrame GetExactlyStackFrame(StackFrame[] stackFrameArray)
        {
            if (stackFrameArray == null)
                return null;

            for (int i = stackFrameArray.Length - 1; i > -1; i--)
                if (!stackFrameArray[i].GetFileName().Contains("Assert"))
                    return stackFrameArray[i];

            return null;
        }

        private static void ShowAssert(Exception ex)
        {
            var stackTrace = new StackTrace(ex, true);
            var stackFrames = stackTrace.GetFrames();
#if DEBUG
            foreach (var sf in stackFrames)
                DiagDbg.WriteLine(sf);
#endif
            var stackFrame = (GetExactlyStackFrame(stackFrames)/*stackFrames.FirstOrDefault<Diagnostics.StackFrame>()*/ ?? new StackFrame());
            var formatedMessage = string.Format("{0} [\"{1}\":{2}]", ex.Message, stackFrame.GetFileName(), stackFrame.GetFileLineNumber());
            Console.WriteLine(formatedMessage);
            DiagDbg.WriteLine(formatedMessage);
        }

        public static void DebugAssert(string message, bool validation = true, bool isDebugAssert = false, string defaultMessage = "Error!")
        {
            if (!validation)
            {
                try
                {
                    throw new Exception((message ?? defaultMessage) ?? "Error!");
                }
                catch (Exception ex)
                {
                    ShowAssert(ex);
                }
            }
        }

        public static void DebugAssert(string message, bool isDebugAssert = false)
        {
            DiagDbg.WriteLine(message ?? (message = "<no dbg message>"));
            if (!isDebugAssert)
            {
                Console.WriteLine(message);
            }           
        }

        public static bool DebugTraceAssert(Delegate traceFunction, string message, object[] traceFunctionArgs = null, 
            bool validation = true, bool isDebugAssert = false, string defaultMessage = "Error!")
        {
            try
            {
                if (traceFunctionArgs == null)
                    traceFunction.DynamicInvoke();
                else traceFunction.DynamicInvoke(traceFunctionArgs);
            }
            catch (Exception ex)
            {
                ShowAssert(ex);
                return false;
            }

            if (!validation)
            {
                try
                {
                    throw new Exception(message ?? defaultMessage);
                }
                catch (Exception ex)
                {
                    ShowAssert(ex);
                    return false;
                }
            }

            return true;
        }

        public static bool DebugCodeAssert<T>(Action<T> code, params object[] codeArgs)
        {
            try
            {
                // enough code.DynamicInvoke(codeArgs) but for just in case!
                if (codeArgs == null)
                    code.DynamicInvoke();
                else code.DynamicInvoke(codeArgs);
            }
            catch (Exception ex)
            {
                ShowAssert(ex);
                return false;
            }

            return true;
        }

        public static T DebugReturnCodeAssert<T>(Func<object[], T> code,
            Func<object[], bool> defaultValidation = null,
            //object[] defaultValidationArgs = null, 
            string defaultExceptionMessage = "Error!",
            params object[] codeArgs)
        {
            try
            {
                var codeResult = code(codeArgs);
                if ((defaultValidation != null) && !defaultValidation(null /*defaultValidationArgs*/))
                    throw new Exception(defaultExceptionMessage ?? "Error!");
                return codeResult;
            }
            catch (Exception ex)
            {
                ShowAssert(ex);
            }

            return default(T);
        }

        // This should be enough when problems shown
        public static T DebugReturnCodeAssert<T>(Func<object[], T> code, params object[] codeArgs)
        {
            try
            {
                return code(codeArgs);
            }
            catch (Exception ex)
            {
                ShowAssert(ex);
            }

            return default(T);
        }

        // Mini dbg
        public static T DebugReturnCodeAssert<T>(Func<T> code)
        {
            try
            {
                return code();
            }
            catch (Exception ex)
            {
                ShowAssert(ex);
            }

            return default(T);
        }

        // ex type dbg
        /*public static T DebugReturnCodeAssert<T>(Func<T> code, Type codex)
        {
            try
            {
                return code();
            }
            catch (typeof(codex) ex)
            {
                ShowAssert(ex);
            }

            return default(T);
        }*/
    }
}
