﻿namespace BasicProcInfo
{
    public static class BPI_Messages
    {
        public static string BasicProcInfoMessage = @"Usage:" + System.Environment.NewLine + 
            @"\tbpi <PID>" + System.Environment.NewLine +
            @"Example:" + System.Environment.NewLine +
            @"\tbpi 1234" + System.Environment.NewLine;
        public static string BasicProcInfoPIDError = @"Cannot convert passed PID to valid PID!";

        public static string BasicProcInfoProcError = @"Cannot find process with passed PID!";
    }
}
