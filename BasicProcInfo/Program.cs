﻿using System;
using BasicProcInfo.Debug;
using System.Diagnostics;

namespace BasicProcInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Asserts.DebugAssert(BPI_Messages.BasicProcInfoMessage, false);
            }
            else
            {
                var pID = 0m;
                object[] codeArgs = new object[] { args[0] };
                Func<object[], bool> codeFunc = (object[] pArgs) => //{ return decimal.TryParse(pArgs[0] as string, out pID) && (pID > 0m); }
                {
                    var funcResult = decimal.TryParse(pArgs[0] as string, out pID);
                    if (pID < 1m)
                        throw new Exception(BPI_Messages.BasicProcInfoPIDError);
                    return funcResult;
                }; //, codeValidFunc = (object[] pArgs) => { return pID > 0m; };
                if (!Asserts.DebugReturnCodeAssert<bool>(codeFunc, codeArgs)) //, codeValidFunc, BPI_Messages.BasicProcInfoPIDError, codeArgs))
                    return;

                var proc = null as Process;
                codeFunc = (object[] pArgs) =>
                {
                    Process.EnterDebugMode();
                    proc = Process.GetProcessById((int)(pArgs[0] as decimal?).Value);
                    if(proc == null)
                        throw new Exception(BPI_Messages.BasicProcInfoProcError);
                    return true;
                };
                codeArgs = new object[] { pID };
                if (!Asserts.DebugReturnCodeAssert<bool>(codeFunc, codeArgs))
                    return;

                var procMainModule = null as ProcessModule;
                var procInfo = null as string;
                codeFunc = (object[] pArgs) =>
                {
                    procMainModule = proc.MainModule;
                    procInfo = string.Format("ProcName: {0},\r\nMachineName: {1},\r\nMMBaseAddr: {2},\r\nMMEntryPointAddr: {3},\r\nMMFileName: \"{4}\",\r\nMMMemSize: {5}\r\n" +
                        "ProcVMem: {6},\r\nMWndHandle: {7},\r\nProcHandle: {8}", 
                        proc.ProcessName, proc.MachineName, procMainModule.BaseAddress, procMainModule.EntryPointAddress,
                        procMainModule.FileName, procMainModule.ModuleMemorySize, proc.VirtualMemorySize64, proc.MainWindowHandle, proc.Handle);
                    return true;
                };
                if (!Asserts.DebugReturnCodeAssert<bool>(codeFunc, (codeArgs = null)))
                {
                    ; // TODO: minimalize informations
                }
                Asserts.DebugAssert(procInfo, false);
            }
        }
    }
}
